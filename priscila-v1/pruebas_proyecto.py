#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: priscila
"""

from urllib.request import urlopen
import urllib
import urllib.request
from bs4 import BeautifulSoup

# DIRE_BASE es la direccion de la pagina de busquedas laborales
# donde se ha borrado el numero que indica desde que anuncio se empieza a mostrar
# que es el numero que va despues de ....start=
dire_base = "https://www.indeed.es/jobs?q=&l=Espa%C3%B1a&start="

# N es el numero de paginas de la busqueda que se van a escrapear
N = 3

# LISTA_TRABAJOS es una lista en blanco donde se irán agregando los trabajos que se escrapean
lista_trabajos = []

for n in range(N):
    dire = dire_base + str(n)
    coso = urllib.request.urlopen(dire)
    sopa = BeautifulSoup (coso.read(),"lxml")
    
    conjunto = sopa.findAll('div',class_='title')    
    
    for j in conjunto:

        lista_trabajos.append(j.get_text())

print('Lista en python de los trabajos')
print(lista_trabajos)

# .rstrip('\n')