#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: priscila
"""

from urllib.request import urlopen
import urllib
import urllib.request
from bs4 import BeautifulSoup

# DIRE_BASE es la direccion de la pagina de busquedas laborales
# donde se ha borrado el numero que indica desde que anuncio se empieza a mostrar
# que es el numero que va despues de ....start=
dire_base = "https://www.indeed.es/jobs?q=&l=Espa%C3%B1a&start="

# N es el numero de paginas de la busqueda que se van a escrapear
N = 3

# LISTA_TRABAJOS es una lista en blanco donde se irán agregando los trabajos que se escrapean
lista_trabajos = []

for n in range(N):
    dire_pagina_n = dire_base + str(n) # agrego el numero de trabajo a partir del que se debe mostrar buscar en la pagina
    coso = urllib.request.urlopen(dire_pagina_n)
    sopa = BeautifulSoup (coso.read(),"lxml")
    
    conjunto_trabajos_pagina_n = sopa.findAll('div',class_='title')    
    
    for j in conjunto_trabajos_pagina_n:
        
        texto = j.get_text() # extraemos el texto del elemento J del conjunto de trabajos de la pagina n
        
        # se usa el metodo .strip('\n') para eliminar los saltos de linea antes de guardar el texto en la lista
        lista_trabajos.append(texto.strip('\n'))

print('Lista en python de los trabajos')
print(lista_trabajos)

# .rstrip('\n')