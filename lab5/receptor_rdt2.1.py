from paquete import *
from constantes import *
from network import *
from socket import *

def create_socket():
	servidor = socket(AF_INET, SOCK_DGRAM)
	servidor.bind((RECEPTOR_IP,RECEPTOR_PORT))
	return servidor

def extract(paquete): 
    data= paquete.get_datos() 
    return data

def deliver_data(message): 
	print(message)

def rdt_rcv(socket): 
	data = socket.recv(2048) 
	emisor,paquete = loads(data) 
	return (emisor,paquete)
	

def corrupto(paquete):
	if calcular_checksum(paquete) == 0:
		return True
	else:
		return False


def make_pkt(data, secuencia): 
	paquete = Paquete(RECEPTOR_PORT , EMISOR_PORT, data, secuencia)
	resultado = calcular_checksum(paquete)  
	paquete.set_checksum(resultado)
	return paquete


def udt_send(socket,emisor,paquete): 
	datos = dumps((emisor,paquete)) 
	socket.sendto(datos,(NETWORK_IP,NETWORK_PORT))
	return (datos)


def close_socket(socket, signal, frame):
	print ("\n\rCerrando socket")
	socket.close()
	exit(0)

# Definimos la funcion para extraer la secuencia de los paquetes que se reciben del receptor
def get_sec(paquete): 
    sec = paquete.get_secuencia() 
    return sec


if __name__ == "__main__":
	servidor= create_socket()# Creamos el socket "receiver"
	# Registramos la senial de salida
	signal.signal(signal.SIGINT, partial(close_socket, servidor))
	print ("listo para recibir mensajes..")# Imprimimos el cartel "Listo para recibir mensajes..."

	secuencia=0
	while True:
		emisor, paquete=rdt_rcv(servidor)
		#print (emisor,paquete)
		sec = get_sec(paquete)
		#print(sec)
		
		
		if secuencia == 0:
			if corrupto(paquete) == 0: 
				pkt = make_pkt("NAK",secuencia) 
				udt_send(servidor,emisor,pkt)
				#print("NAK1") 
			elif (corrupto(paquete) == 1) and (sec == 1):
				pkt = make_pkt("ACK",secuencia) 
				udt_send(servidor,emisor,pkt)
				#print("ACK") 				
				#print("duplicado")
			elif (corrupto(paquete) == 1) and (sec == 0):
				pkt = make_pkt("ACK",secuencia)
				udt_send(servidor,emisor,pkt) #los envia a la red y al emisor

				data= extract(paquete)# Extrae los datos
				deliver_data(data)# Entregamos los datos a la capa de aplicacion
				#print('miercole')
	
				secuencia = (secuencia + 1) % 2 # Actualizo la secuencia
		else:
			if corrupto(paquete) == 0: 
				pkt = make_pkt("NAK",secuencia) 
				udt_send(servidor,emisor,pkt)
				#print("NAK2") 
			elif (corrupto(paquete) == 1) and (sec == 0):
				pkt = make_pkt("ACK",secuencia) 
				udt_send(servidor,emisor,pkt)
				#print("ACK")
				#print("duplicado")
			elif (corrupto(paquete) == 1) and (sec == 1):
				pkt = make_pkt("ACK",secuencia)
				udt_send(servidor,emisor,pkt) #los envia a la red y al emisor

				data= extract(paquete)# Extrae los datos
				deliver_data(data)# Entregamos los datos a la capa de aplicacion
				#print('jueves')
	
				secuencia = (secuencia + 1) % 2 # Actualizo la secuencia

	close_socket(servidor)
