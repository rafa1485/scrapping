#!/usr/bin/env python
# encoding: utf-8
"""
network.py
"""
from paquete import *
from constantes import *
from random import random

def recv_pckt(socket):
	data, emisor = socket.recvfrom(1024)
	receptor, pckt = loads(data)
	return emisor, receptor, pckt

def send_pckt(socket, emisor, receptor, pckt):
	data = dumps((emisor, pckt))
	socket.sendto(data, receptor)

def process_pkt(pckt):
	addrs = (pckt)
	return pckt, addrs

def shut_down(socket, signal, frame):
	socket.close()
	exit(0)

def corruptNet(sock, sender, receiver, pckt):
	print('****************')	
	if (sender == (EMISOR_IP, EMISOR_PORT)):
		prob = int(random()*10)
		if prob < 2:
			pckt.set_checksum(prob)
			print('Envio Corrupto')
		elif 2 <= prob and prob < 3:
			send_pckt(sock, sender, receiver, pckt)
			print('Envio Duplicados')
		else:
			print('OK')
	send_pckt(sock, sender, receiver, pckt)
	print('dato enviado:  ')
	print(pckt.get_datos())

if __name__ == "__main__":
	# Creamos el socket para la red
	sock = socket(AF_INET, SOCK_DGRAM)
	# Lo ligamos a su direccion
	sock.bind((NETWORK_IP, NETWORK_PORT))
	# Registramos la senial de salida
	signal.signal(signal.SIGINT, partial(shut_down, sock))
	# Imprimimos mensaje
	print('Red Habilitada')
    
	while True:
		sender, receiver, pckt = recv_pckt(sock)
		print("recv_pckt")
		pckt = corruptNet(sock, sender, receiver, pckt)
		print("send_pckt")

