from constantes import *
from socket import *
from paquete import *
from network import *

def create_socket():
	UDPsocket = socket(AF_INET, SOCK_DGRAM) 
	UDPsocket.bind((EMISOR_IP,EMISOR_PORT))
	return UDPsocket


def rdt_send():
    data=input('ingrese un mensaje:  ')
    return(data.encode('utf-8'))


def make_pkt(data, secuencia):
    paquete= Paquete(EMISOR_PORT , RECEPTOR_PORT, data, secuencia)
    cksum = calcular_checksum(paquete)
    paquete.set_checksum(cksum)
    return paquete  
	

def udp_send(socket, mensaje, receiver): 
	mensaje=dumps((receiver, mensaje)) 
	socket.sendto(mensaje, (NETWORK_IP,NETWORK_PORT))



def close_socket(socket, signal, frame):
	print ("\n\rCerrando socket")
	socket.close()
	exit(0)

# Le damos al emisor la funcion para recibir
def rdt_rcv(socket): 	
	data = socket.recv(2048)
	emisor,paquete = loads(data)
	 
	return (emisor,paquete)

# Definimos la funcion para extraer los datos de los paquetes que se reciben del receptor
def extract(paquete): 
    data= paquete.get_datos() 
    return data

# Definimos la funcion para extraer la secuencia de los paquetes que se reciben del receptor
def get_sec(paquete): 
    sec = paquete.get_secuencia() 
    return sec


if __name__ == "__main__":

	cliente=create_socket() # Creamos el socket
	
	signal.signal(signal.SIGINT, partial(close_socket, cliente))#esta funcion toma el socketal final

	secuencia = 0
    
	while True: # Iteramos indefinidamente
		data=rdt_send() # Leemos el mensaje desde teclado
		
		if secuencia == 0:
			paquete=make_pkt(data, 0) # Hacemos el paquete
			destinatario = (RECEPTOR_IP, RECEPTOR_PORT) #define el destino en el
								    # que deberia llegar el packet
			flag = True # Flag que se hace True cuando el receptor 
			while flag:
				udp_send(cliente, paquete, destinatario) # Enviamos a la capa red
				emisor, paquete2 =rdt_rcv(cliente)
				data2 = extract(paquete2) # Extrae los datos
						 
				# Imprimimos el ACK (si el archivo se recibio correctamente)
				# o NAK (si el archivo recibido estaba corrupto)				
				#print(data2)
		
				# Si el paquete llega corrupto, entonces enviamos de nuevo el paquete
				if data2 == 'NAK':
					flag = True
				else:
					flag = False
			secuencia = (secuencia + 1) % 2

		else: 
			paquete=make_pkt(data, 1) # Hacemos el paquete
			destinatario = (RECEPTOR_IP, RECEPTOR_PORT) #define el destino en el
								    # que deberia llegar el packet
			flag = True # Flag que se hace True cuando el receptor 		
			while flag:
				udp_send(cliente, paquete, destinatario) # Enviamos a la capa red
				emisor, paquete2 =rdt_rcv(cliente)
				data2 = extract(paquete2) # Extrae los datos
						 
				# Imprimimos el ACK (si el archivo se recibio correctamente)
				# o NAK (si el archivo recibido estaba corrupto)				
				#print(data2)
		
				# Si el paquete llega corrupto, entonces enviamos de nuevo el paquete
				if data2 == 'NAK':
					flag = True
				else:
					flag = False
			secuencia = (secuencia + 1) % 2

	close_socket(cliente)  
